from typing import Optional

from django.conf import settings
import requests

FAST_2_SMS_BASE_URL = "https://www.fast2sms.com/dev/bulkV2"
NEW_COMPLAINT = 'New'
ASSIGNED_COMPLAINT = 'Assigned'
OUT_FOR_SERVICE_COMPLAINT = 'In Travelling'
ONGOING_COMPLAINT = 'Ongoing'

RESCHEDULED_SELF_COMPLAINT = 'Rescheduled for later'
RESCHEDULED_COMPLAINT = 'Rescheduled'
REPLACEMENT_COMPLAINT = 'Replacement Requested'
REPLACEMENT_COMPLAINT_APPROVED = 'Replacement Approved'
COMPLETED_COMPLAINT = 'Completed'

STATUS_MESSAGE = {
    NEW_COMPLAINT: "A new complaint has been registered. REF : {}",
    ASSIGNED_COMPLAINT: "",
    OUT_FOR_SERVICE_COMPLAINT: "",
    ONGOING_COMPLAINT: "",
    RESCHEDULED_SELF_COMPLAINT: "",
    RESCHEDULED_COMPLAINT: "",
    REPLACEMENT_COMPLAINT: "",
    REPLACEMENT_COMPLAINT_APPROVED: "",
    COMPLETED_COMPLAINT: "",
}


class Fast2SMS:
    def __init__(self):
        self.url = FAST_2_SMS_BASE_URL

    def send_p_sms(self, phone_no, message):
        url = self.url
        print("SMS INITIALIZED", phone_no, message)

        querystring = {
            "authorization": settings.FAST_2_SMS_API_KEY,
            # "sender_id": getattr(settings, 'FAST_2_SMS_SENDER_ID', "SMSINI"),
            "sender_id": "TXTIND",
            # "language": "english",
            "route": "v3",
            "numbers": phone_no,
            "message": message,
        }
        headers = {'cache-control': "no-cache"}
        response = requests.request("GET", url, headers=headers, params=querystring)
        print(response.text, "$$$ sent $$$$")
        return True

    def send_qt_sms(self, phone_no, message):
        url = self.url

        querystring = {
            "authorization": settings.FAST_2_SMS_API_KEY,
            "sender_id": "TXTIND",
            # "sender_id": settings.FAST_2_SMS_SENDER_ID,
            "language": "english",
            "route": "dlt",
            "numbers": phone_no,
            "message": settings.FAST_2_SMS_TEMPLATE_ID,
            "variables_values": "%s" % message
        }
        headers = {'cache-control': "no-cache"}
        response = requests.request("GET", url, headers=headers, params=querystring)
        print(response.text)
        return True


def send_p_sms(phone_no, message):
    print("calling fast 2 class")
    return Fast2SMS().send_p_sms(phone_no, message)


def send_qt_sms(phone_no, message):
    return Fast2SMS().send_qt_sms(phone_no, message)
