"""survey URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from survey_app.admin_views import views

router = routers.SimpleRouter()
# router.register(r'company', views.CompanyViewSet),
# router.register(r'questions/<int:id>/', views.QuestionViewSet),


def redirect_to_admin(request, pk):
    if request.user.is_authenticated:
        return HttpResponseRedirect(f"/admin/survey_app/group/{pk}/change/")
    return HttpResponseRedirect(f"/admin/login/?next=/admin/survey_app/group/{pk}/change/")

urlpatterns = [
    path('api/', include('survey_app.api.urls')),
    path('', views.login, name='login'),
    path('d/<int:pk>', redirect_to_admin, name='redirect_to_admin'),
    path('dashboard/', include('survey_app.admin_views.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest-framework')),
    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)