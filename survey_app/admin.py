from django.contrib import admin
from .models import *
from . models import Group as group_
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

# Register your models here.


class MyUserAdmin(UserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = User
    list_display = ['username', 'email', 'branch']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('branch', )}),
    )


class SurveyAdmin(admin.ModelAdmin):
    list_display = ('name', 'branch', 'sheet_link', 'concerned_person_contact', 'sms_sent_count', 'url_opened_count')
    readonly_fields = ("spreadsheet_id", "sheet_link", 'sms_sent_count', 'url_opened_count', )


class OptionInline(admin.TabularInline):
    model = Option


class AnswerInline(admin.TabularInline):
    model = Answer


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'survey')
    inlines = [OptionInline, ]


class GroupAdmin(admin.ModelAdmin):
    list_display = ('reference', 'survey', )
    readonly_fields = ('answers', )
    # inlines = [AnswerInline, ]


admin.site.register(User, MyUserAdmin)
admin.site.register(Company)
admin.site.register(Branch)
admin.site.register(Survey, SurveyAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(Option)
# admin.site.unregister(Group)
admin.site.register(group_, GroupAdmin)











