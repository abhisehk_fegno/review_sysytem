from survey_app.models import *
from django.forms import ModelForm, inlineformset_factory, Select, ChoiceField


class QuestionForm(ModelForm):
    QUESTION_TYPE = (
        ('text', 'text'),
        ('select', 'select '),
        ('radio', 'radio '),
        ('rating_radio', 'rating_radio'),
        ('description', 'description '),
        ('checkbox', 'checkbox '),
        ('email', 'email'),
        ('boolean', 'boolean')

    )

    # type = ChoiceField(choices=QUESTION_TYPE)
    class Meta:
        model = Question
        fields = ['title', 'type', 'survey']
        widgets = {
            'type': Select(attrs={'class': 'form-select'}),
            'survey': Select(attrs={'class': 'form-select'}),
        }

    # def __init__(self, *args, **kwargs):
    #     self.fields['type'].widget = Select2Widget()
    #     self.fields['survey'].widget = Select2Widget()
        
        
class OptionForm(ModelForm):
    class Meta:
        model = Option
        fields = ['value', 'image']
    
    # def __init__(self, *args, **kwargs):
    #     self.fields['current_user'].widget = Select2Widget()
        
OptionFormSet = inlineformset_factory(Question, Option, form=OptionForm, extra=5)
