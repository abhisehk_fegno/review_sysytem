from . views import *
from django.urls import path, include
from django.contrib.auth import views as auth_views

urlpatterns = [

    path('index/', index, name='index'),
    path('', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='login.html'), name='logout'),
    path('add/question/', QuestionCreate.as_view(), name='add-question'),
    path('question/<int:id>/', QuestionList.as_view(), name='question-list'),
    path('add/options/', OptionCreate.as_view(), name='add-option'),
    path('add/branch/', BranchCreate.as_view(), name='add-branch'),
    path('add/survey/', SurveyCreate.as_view(), name='add-survey'),
    path('latest/review/', LatestReview.as_view(), name='latest-review'),
    path('latest/review/<int:pk>/', LatestReviewDetail.as_view(), name='latest-review-detail'),
    path('rev/<int:pk>/load/', ReviewExtLinkRedirection.as_view(), name='rev-load'),

]

