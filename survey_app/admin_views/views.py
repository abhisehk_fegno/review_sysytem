from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView
from django.contrib.auth.decorators import login_required
from ..models import Question, Branch, Answer, Survey, Option
from django.utils.decorators import method_decorator 
from .forms import QuestionForm, OptionForm, OptionFormSet
from django.db import transaction
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.db.models.functions import TruncDate
from itertools import groupby
from operator import attrgetter


@login_required(login_url='login')
def index(request):
    return render(request, 'index.html')


def login(request):
    return redirect('/dashboard/')


class QuestionList(ListView):
    model = Question
    template_name = 'survey_app/question_form.html'
    paginate_by = 10

    def get_context_data(self, *args, **kwargs):
        data = super(QuestionList, self).get_context_data(*args, **kwargs)
        page = self.request.GET.get('page', 1)
        paginator = self.paginator_class(Question.objects.filter(survey_id=self.kwargs['id']).order_by('id'),
                                         self.paginate_by
                                         )
        data['objects'] = paginator.page(page)
        data['form1'] = QuestionForm
        data['form2'] = OptionFormSet
        return data
    
    
class QuestionCreate(CreateView):
    model = Question
    fields = ['title',  'type', 'survey']
    template_name = 'survey_app/question_form.html'
    # form_class = QuestionForm
    
    def get_context_data(self, **kwargs):
        data = super(QuestionCreate, self).get_context_data(**kwargs)
        # import pdb;pdb.set_trace()
        
        if self.request.POST:
            data['form1'] = QuestionForm
            data['form2'] = OptionFormSet(self.request.POST, self.request.FILES)
        else:
            data['form1'] = QuestionForm
            data['form2'] = OptionFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        options = context['form2']
        with transaction.atomic():
            # import pdb;
            # pdb.set_trace()
            self.object = form.save()
            if options.is_valid():
                options.instance = self.object
                options.save()
        return super(QuestionCreate, self).form_valid(form)


class OptionCreate(CreateView):
    model = Option
    fields = ['question', 'value', 'image']
    template_name = 'survey_app/form.html'


class BranchCreate(CreateView):
    model = Branch
    fields = ['name', 'company', 'logo', 'google_place_id']
    template_name = 'survey_app/form.html'


class SurveyCreate(CreateView):
    model = Survey
    fields = ['name', 'branch']
    template_name = 'survey_app/survey_list.html'
    
    def get_context_data(self):
        context = super().get_context_data()
        context['objects'] = Survey.objects.all()
        return context


class LatestReview(ListView):
    model = Answer
    queryset = Answer.objects.all().order_by('-created_at')
    fields = ['question', 'original_answer', 'option']
    template_name = 'survey_app/overall_list.html'
    context_object_name = 'reviews'
    

class LatestReviewDetail(ListView):
    model = Answer
    fields = ['question', 'original_answer', 'option']
    template_name = 'survey_app/list.html'
    context_object_name = 'reviews'
    
    def get_context_data(self, **kwargs):
        context = super(LatestReviewDetail, self).get_context_data(**kwargs)
        ans = Answer.objects.annotate(created_at_date=TruncDate('created_at')).order_by('created_at')
        group = groupby(ans, attrgetter('created_at_date'))
        print(type(ans))
        for date in ans:
            print(date, date.created_at)
        try:
            reviews = Answer.objects.filter(question_id=self.kwargs['pk']).order_by('-created_at')
            context['reviews'] = reviews

        except Answer.DoesNotExist:
            print(messages.error(self.request, "Does not have Option !!!"))
        return context


class ReviewExtLinkRedirection(SingleObjectMixin, RedirectView):
    queryset = Survey.objects.all()

    def get_redirect_url(self, *args, **kwargs):
        obj = self.get_object()
        if obj.review_redirect_url:
            obj.url_opened_count += 1
            obj.save()
            return obj.review_redirect_url
        return
