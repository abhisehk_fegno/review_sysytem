from django.contrib.sessions.middleware import SessionMiddleware

class CsrfExemptMiddleware(SessionMiddleware):
    def process_request(self, request):
        attr = '_dont_enforce_csrf_checks'
        if not getattr(request, attr, False):
            setattr(request, attr, True)
        
