from rest_framework import serializers
from rest_framework.response import Response
from django.core import serializers as serializers_

from ..models import *


# ==========================for review submission=========================


class SurveySerializer(serializers.ModelSerializer):
    review_redirection_handle_url = serializers.SerializerMethodField()

    def get_review_redirection_handle_url(self, instance):
        if 'request' in self.context:
            return self.context['request'].build_absolute_uri(
                reverse('rev-load', kwargs={"pk": instance.id})
            )
        return reverse('rev-load', kwargs={"pk": instance.id})

    class Meta:
        model = Survey
        fields = ('id', "name", "enable", "review_redirection_handle_url")


class BranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        fields = ('name', 'company', 'logo', 'google_place_id')


class UserSigninSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ['username', 'password']

# ============================= For question listing ===========================


class OptionSerializer(serializers.ModelSerializer):
    image_url = serializers.SerializerMethodField()

    def get_image_url(self, instance):
        if not instance.image:
            return None
        else:
            request = self.context['request']
            photo_url = instance.image.url
            return request.build_absolute_uri(photo_url)

    class Meta:
        model = Option
        fields = ('label', 'id', 'value', 'image_url', 'display_order')


class AnswerSerializer(serializers.ModelSerializer):
    def validate_answer(self, errors):
        d = {}
        if not self.validated_data['original_answer']:
            if not self.validated_data['option']:
                d = {'required': False}
                errors.update({self.validated_data['question'].id: "Field is empty"})

            type = self.validated_data['question'].type
            for i in range(len(self.validated_data['option'])):
                if type == 'rating_radio':
                    self.validated_data['original_answer'] = self.validated_data['option'][0].value
                    continue
                self.validated_data['original_answer'] += str(self.validated_data['option'][i]) + ", "
        return errors, d

    class Meta:
        model = Answer
        fields = ('original_answer', 'question', 'option', 'group')


class QuestionSerializer(serializers.ModelSerializer):
    option = OptionSerializer(read_only=True, many=True, source='option_set')
    label = serializers.SerializerMethodField(read_only=True)
    show_links_if_rating_in_marked = serializers.SerializerMethodField()

    def get_show_links_if_rating_in_marked(self, instance):
        if instance.show_links_if_rating_in_marked:
            values = instance.show_links_if_rating_in_marked.replace('\r', '').split('\n')
            return instance.options.all().filter(
                value__in=values
            ).values_list('id', flat=True)
        return []

    def get_label(self, instance):
        return instance.description

    class Meta:
        model = Question
        fields = ('id', 'title', 'label', 'required', 'display_order', 'survey', 'type', 'option',
                  'show_links_if_rating_in_marked',)

