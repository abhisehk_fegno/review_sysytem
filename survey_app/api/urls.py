from django.urls import path, include
from .views import *
from rest_framework import routers
router = routers.SimpleRouter()

urlpatterns = [
    path('', api_root, name='api-root'),
    path('branch/<int:id>/', BranchViewSet.as_view(), name='branch'),
    path('survey/<int:id>/', SurveyViewSet.as_view(), name='survey'),
    path('questions/-1/', QuestionViewSet.as_view(), name='question'),
    path('questions/<int:q>/', QuestionViewSet.as_view(), name='question'),
    path('submit/', ReviewViewSet.as_view(), name='submit'),
    path('signin/', signin, name='signin'),
]
