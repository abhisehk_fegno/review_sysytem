from django.contrib.auth import authenticate
from django.db.models import Prefetch
from django.views.generic import RedirectView, UpdateView
from django.views.generic.detail import SingleObjectMixin
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes, api_view
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.generics import ListAPIView
from rest_framework import status
from django.http import Http404
# from .middleware import CsrfExemptMIddleware
from .serializers import *
from .authentication import token_expire_handler, expires_in
from django.db.transaction import atomic
from ..models import *
from rest_framework.authentication import BasicAuthentication 
from django.shortcuts import get_object_or_404


@api_view(('GET',))
def api_root(request):
    data = {
        "username": "marketing_emp1",
        "password": "mkasdf1234"
    }
    return Response(data=data, status=status.HTTP_200_OK)


class QuestionViewSet(ListAPIView):
    queryset = Question.objects.prefetch_related(Prefetch('option_set', queryset=Option.objects.select_related('question')))
    serializer_class = QuestionSerializer

    def get_review_redirection_handle_url(self, instance):
        return self.request.build_absolute_uri(
            reverse('rev-load', kwargs={"pk": instance.id})
        )

    def list(self, request, *args, **kwargs):
        # print(request.META.get('HTTP_AUTHORIZATION'))
        logo = ""
        _id = self.kwargs.get('id')
        if _id is None:
            survey = Survey.objects.filter(enable=True).order_by('id').last()
        else:
            survey = get_object_or_404(Survey, id=_id)

        logo = request.build_absolute_uri(survey.branch.company.logo.url)
        qs = self.get_queryset().filter(survey=survey)
        result = self.get_serializer(qs, many=True, context={'request': request}).data
        message = ""

        return Response({"logo": logo, "title": str(survey), "questions": result, "message": message,
                         "review_redirection_url": self.get_review_redirection_handle_url(survey)})

        # result = {}


class ReviewViewSet(CreateAPIView):
    queryset = Answer.objects.prefetch_related(Prefetch('option', queryset=Option.objects.select_related('question')))
    serializer_class = AnswerSerializer
    # authentication_classes = (CsrfExemptMIddleware, BasicAuthentication)

    @atomic()
    def post(self, request, *args, **kwargs):
        errors = {}
        out = {}
        non_field_errors = False
        # import pdb;pdb.set_trace()
        print(request.data.get("review_list"))
        g = Group.objects.create()
        instances_set = []
        for submissions in request.data.get("review_list", []):
            ser = self.get_serializer(data=submissions)
            if ser.is_valid():
                errors, d = ser.validate_answer(errors)
                print(d)
                if not d:
                    ser.validated_data['group'] = g
                    if not g.survey:
                        g.survey = ser.validated_data['question'].survey
                        g.save()
                    ser.save()
                    out[ser.validated_data['question'].id] = {
                        'question': ser.validated_data['question'].id,
                        'original_answer': ser.validated_data['original_answer'],
                        'group': g.id,
                        'options': [{
                            'id': opt.id,
                            'value': opt.value,
                            'label': opt.label,
                            'file': request.build_absolute_uri(opt.image.url) if opt.image else None,
                        } for opt in ser.validated_data["option"]]

                    }
                if ser.errors:
                    non_field_errors = True
            else:
                # import pdb;pdb.set_trace()
                errors["field_errors"] = ser.errors
        return Response({
            'errors': errors,
            'non_field_errors': non_field_errors,
            'error_count': len(errors.keys()),
            'out': out,
            'out_count': len(out.keys()),
        })


class BranchViewSet(ListAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer

    def filter_queryset(self, queryset):
        # import pdb; pdb.set_trace()
        return queryset.filter(company=self.kwargs['id'])


class SurveyViewSet(ListAPIView):
    queryset = Survey.objects.all()
    serializer_class = SurveySerializer

    # lookup_field = 'branch'

    def filter_queryset(self, queryset):
        return queryset.filter(branch=self.kwargs['id'])


# =======================token authentication====================


@api_view(["POST"])
@permission_classes((AllowAny,))  # here we specify permission by default we set IsAuthenticated
def signin(request):
    signin_serializer = UserSigninSerializer(data=request.data)
    if not signin_serializer.is_valid():
        return Response(signin_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    user = authenticate(
        username=signin_serializer.data['username'],
        password=signin_serializer.data['password']
    )
    # import pdb;pdb.set_trace()
    if not user:
        return Response({'detail': 'Invalid Credentials or activate account'}, status=status.HTTP_404_NOT_FOUND)

    # TOKEN STUFF
    token, _ = Token.objects.get_or_create(user=user)

    # token_expire_handler will check, if the token is expired it will generate new one
    is_expired, token = token_expire_handler(token)  # The implementation will be described further
    # user_serialized = UserSerializer(user)

    return Response({
        # 'user': user,
        'expires_in': expires_in(token),
        'token': token.key

    })


