from __future__ import print_function
import os.path
import pickle

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
# from google.oauth2.credentials import Credentials
from google.oauth2.service_account import Credentials
import gspread
from django.conf import settings
# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']

# The ID and range of a sample spreadsheet.
# SAMPLE_SPREADSHEET_ID = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
SAMPLE_SPREADSHEET_ID = '1k0oaFf-xqyO3GyGNocbcSWNA99uZveEfJpcMftFAbqE'
SAMPLE_RANGE_NAME = 'Class Data!A2:E'

SERVICE_ACCOUNT_FILE = os.path.join(
    settings.BASE_DIR, "survey_app", "google_sheets",
    "abcemporiogsheets_credentials.json"
)
SERVICE_TOKEN_FILE = (os.path.join(settings.BASE_DIR, 'survey_app', 'google_sheets', 'sheets_token.json'))

def access_sheet():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    # if os.path.exists('token.json'):
    #     creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # # If there are no (valid) credentials available, let the user log in.
    # if not creds or not creds.valid:
    #     if creds and creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow = InstalledAppFlow.from_client_secrets_file(
    #             '/home/fegno/Desktop/review-system-quick/app/google_sheets/sheets_credentials.json', SCOPES)
    #         creds = flow.run_local_server(port=0)
    #     # Save the credentials for the next run
    #     with open('/home/fegno/Desktop/review-system-quick/app/google_sheets/sheets_token.json', 'w') as token:
    #         token.write(creds.to_json())

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print('Name, Major:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print('%s, %s' % (row[0], row[4]))


# access_sheet()

def authorize_google():
    credentials = Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
    gc = gspread.authorize(credentials)
    # with open(SERVICE_TOKEN_FILE, 'wb') as token:
    #     pickle.dump(gc, token)
    return gc


def create_sheet(survey, branch_email=None, ):
    try:
        sh = authorize_google().create(f'{ survey } review spreadsheet')
    except Exception as e:
        print(e)
    else:
        if branch_email:
            sh.share(branch_email, perm_type='user', role='writer')
            print(sh)
        return sh.id


# def write_sheet(key, *args, **kwargs):
#     branch_sheet_id = key
#     values = list(args)[0]
#     question = list(args)[0]
#     # with open(SERVICE_TOKEN_FILE, 'rb') as token:
#     #     token_auth = pickle.load(token)
#     sht1 = authorize_google().open_by_key(branch_sheet_id)
#     sheet1 = sht1.get_worksheet(0)
#     sheet1.update(f"A{1}:C{1}", [["Question", "Answer", "Question_type"]])
#     next_empty_row = len(sheet1.get_all_values()) + 1
#     print(next_empty_row, values)
#     sheet1.update(f"A{next_empty_row}:C{next_empty_row}", [values])
#     print(sheet1)


# def get_sheet(key, branch_email):
#     branch_email = settings.DEFAULT_SHEET_EMAIL
#     branch_sheet_id = key
#     credentials = Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
#     gc = gspread.authorize(credentials)
#     # branch_sheet_id = '1Kag0nuofdri9-Rpg3MJhk0ZdVlvGQ2FBMugoW7cxOPs'
#     sht1 = gc.open_by_key(branch_sheet_id)
# 
# 
#     return sht1
