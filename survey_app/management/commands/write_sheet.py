import os
from collections import OrderedDict
from pprint import pprint
from django.db import connection
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
import gspread
from survey_app.models import Answer, Question, Survey, Group
from google.oauth2.service_account import Credentials
from django.db.models import Prefetch
from gspread_formatting import *

SCOPES = ['https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/drive']
SERVICE_ACCOUNT_FILE = os.path.join(settings.BASE_DIR, 'survey_app', 'google_sheets',
                                    'abcemporiogsheets_credentials.json')


class Command(BaseCommand):
    help = "pass the spreadsheet id with commands"
    gc = None
    # def add_arguments(self, parser):
    #     parser.add_argument('branch', type=str, help='Indicates the branch')

    def write_header(self, survey_obj, row):
        """
        Getting Sheet and adding header if sheet is empty.
        """
        sheet = None
        try:
            workbook = self.gc.open_by_key(survey_obj.spreadsheet_id)
            sheet = workbook.get_worksheet(0)
        except Exception as e:
            print("Can't Access Worksheet. Exiting...")
            return None, False

        if len(sheet.get_all_values()) == 0:
            sheet.format('A1:Z1', {"textFormat": {'bold': False}, "backgroundColor": {
                "red": 0.79,
                "green": 0.85,
                "blue": 0.97
            }})
            header_row = ['Date', 'Time'] + row
            sheet.update('A1:Z1', [header_row])
        return sheet, True

    def handle(self, *args, **kwargs):
        """
        branch_sheet_id = kwargs['key']
        branch = kwargs['branch']
        """

        credentials = Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
        self.gc = gspread.authorize(credentials)

        survey_set = Survey.objects.filter(enable=True).prefetch_related('question_set')
        if not survey_set.exists():
            print("There are not active surveys. Exiting...")
            return

        separator = "="*40

        for obj in survey_set:
            print(separator)

            question_qs = Question.objects.filter(survey=obj).order_by('display_order').values('id', 'title')
            extracted_titles = [q['title'] for q in question_qs]
            sheet, success = self.write_header(survey_obj=obj, row=extracted_titles)
            answer_holder = OrderedDict()
            for q in question_qs:
                question_id = q['id']
                answer_holder[question_id] = None
            if not success:
                continue

            _count = len(connection.queries)

            groups_list = []
            answer_prefetch = Prefetch(
                'answer_set',
                Answer.objects.filter(issubmitted=False).order_by('question__display_order', 'question_id')
            )
            group_set = Group.objects.filter(
                issubmitted=False, survey_id=obj.id
            ).prefetch_related(answer_prefetch).order_by('id')

            for group in group_set:
                q_order = answer_holder.copy()
                for answer in group.answer_set.all():
                    q_order[answer.question_id] = answer.original_answer
                answers_to_write = list(q_order.values())
                row = [
                    # group.reference,
                    group.created_at.strftime('%d-%m-%Y'),
                    group.created_at.strftime('%H:%M %P'),
                    *answers_to_write
                ]
                groups_list.append(row)

            pprint(groups_list)
            print("Executed Queries", len(connection.queries) - _count)
            sheet.append_rows(groups_list)
        Answer.objects.filter(issubmitted=False).update(issubmitted=True)
        Group.objects.filter().update(issubmitted=True)

        # Answer.objects.update(issubmitted=False)
        # Group.objects.filter().update(issubmitted=False)

