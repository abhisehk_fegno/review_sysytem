from itertools import count
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.utils.text import slugify
from django_extensions.db.fields import AutoSlugField

from sms import send_p_sms


class Company(models.Model):
    name = models.CharField(max_length=100)
    logo = models.ImageField(upload_to='logo', null=True, blank=True)

    def __str__(self):
        return self.name


class Branch(models.Model):
    name = models.CharField(max_length=100)
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, blank=True)
    logo = models.ImageField(upload_to='logo', null=True, blank=True)
    email = models.EmailField(null=True, help_text="a gmail id to which the sheet is shared!")
    google_place_id = models.CharField(max_length=70, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('add-branch', kwargs={})


class User(AbstractUser):
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.username


class Survey(models.Model):
    name = models.CharField(max_length=100)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    spreadsheet_id = models.CharField(max_length=200, blank=True, null=True)
    enable = models.BooleanField(default=True)
    review_redirect_url = models.URLField(null=True, blank=True)
    url_opened_count = models.PositiveIntegerField(default=0)
    sms_sent_count = models.PositiveIntegerField(default=0)
    concerned_person_contact = models.CharField(
        null=True, max_length=10, help_text="10 digit mobile number to receive sms."
    )

    def sheet_link(self):
        if self.spreadsheet_id:
            link = f"https://docs.google.com/spreadsheets/d/{self.spreadsheet_id}/view#gid=0"
            return mark_safe(f"""<a href="{link}" target="blank">GSheet:{self.spreadsheet_id}</a>""")
        return ''

    sheet_link.short_description = "Google Sheet Link"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('add-survey')


class Question(models.Model):

    QUESTION_TYPE = (
        ('text', 'Text'),
        ('select', 'Select'),
        ('radio', 'Radio Option'),
        ('rating_radio', 'Rating Radio'),
        ('description', 'Description '),
        ('checkbox', 'Checkbox'),
        ('email', 'Email'),
        ('boolean', 'Boolean')
    )

    title = models.CharField(max_length=500)
    description = models.TextField(max_length=1000, default='')
    display_order = models.IntegerField(default=50)
    type = models.CharField(max_length=15, choices=QUESTION_TYPE)
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    required = models.BooleanField(default=True)
    send_sms_if_rating_in_marked = models.TextField(
        null=True,
        blank=True,
        help_text="Enter each input on each line; "
                  "so that, if user marked answer is something in this list, will send an sms to you!"
    )

    show_links_if_rating_in_marked = models.TextField(
        null=True,
        blank=True,
        help_text="Enter each input on each line; "
                  "so that, if user will be shown a link to review externally if answer is something in this list!"
    )

    question_slug = AutoSlugField(max_length=1000, null=True, blank=True, populate_from=('title',))
    is_new = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.description:
            self.description = str(self.title)
        super(Question, self).save(*args, **kwargs)
        self.generate_options()

    def generate_options(self):
        if self.options.all().exists():
            return
        if self.type == 'rating_radio':
            options = [
                Option(
                    question=self, value="Bad", label="Bad",
                    display_order=1, image="option_images/bad.png"),
                Option(
                    question=self, value="Average", label="Average",
                    display_order=2, image="option_images/average.png"),
                Option(
                    question=self, value="Good", label="Good",
                    display_order=3, image="option_images/good.png"),
                Option(
                    question=self, value="Awesome", label="Awesome",
                    display_order=4, image="option_images/awesome.png"),
            ]
            Option.objects.bulk_create(options)
        if self.type == 'boolean':
            options = [
                Option(question=self, value="Yes", label="Yes", display_order=1),
                Option(question=self, value="No", label="No", display_order=2),
            ]
            Option.objects.bulk_create(options)

        return

    def get_absolute_url(self):
        return reverse('add-question')

    @property
    def options(self):
        return Option.objects.filter(question=self)

    class Meta:
        ordering = ('-display_order', '-id')


class Option(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    value = models.CharField(max_length=200)
    image = models.ImageField(upload_to='option_images', null=True, blank=True)
    label = models.CharField(max_length=200, null=True, blank=True)
    display_order = models.IntegerField(default=50, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.label:
            self.label = self.value
        return super(Option, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('add-option')

    def __str__(self):
        return f"{self.value}"

    class Meta:
        ordering = ('display_order', '-id')


class Group(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    issubmitted = models.BooleanField(default=False)
    survey = models.ForeignKey(Survey, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.reference

    def answers(self):
        tr = ""
        index = 3
        for ans in self.answer_set.all().select_related('question').order_by('question__display_order'):
            tr += f"""<tr>
                <td>{index}</td>
                <td>{ans.question and ans.question.title}</td>
                <td title="{ans.answer_only}">{ans.answer_as_html}</td>
            </tr>"""
            index += 1
        template_string = f"""
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <style>.checked {{color: orange;}} </style>
            <div class="responsive">
            <table class="table table-responsive " style="width:100%">
                <tbody>
                    <tr><td>1</td><td scope="row">Reference ID</td> <td>{self.reference}</td></tr>
                    <tr><td>2</td><td>Survey ID</td><td>{self.survey and self.survey.name}</td></tr>
                </tbody>
            </table>
            &nbsp;
            <table class="table table-responsive " style="width:100%">

                <thead class="thead-dark">
                    <tr><th>#</th> <th scope="col">Question</th> <th scope="col">Answer</th> </tr>
                </thead>
                <tbody>   
                    {tr}
                </tbody>
            </table>
            </div>
            """
        return mark_safe(template_string)

    @property
    def reference(self):
        if self.survey_id:
            return f"#RSP{self.survey_id}_{self.id}"
        return f"#RSP-TMP_{self.id}"

    class Meta:
        verbose_name = 'Response'


class Answer(models.Model):
    CHOICES = (
        ('read', 'read'),
        ('write', 'write')
    )
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    original_answer = models.TextField(max_length=500, null=True, blank=True)
    option = models.ManyToManyField(Option, related_name="radio", blank=True)
    display_order = models.IntegerField(default=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    email = models.CharField(max_length=20, null=True, blank=True)

    issubmitted = models.BooleanField(default=False)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)

    def save(self, *args, **kwargs):
        if self.pk and self.option and not self.original_answer:
            ans = self.__sep__.join(self.option.all().values_list('value', flat=True))
            self.original_answer = ans
        return super(Answer, self).save(*args, **kwargs)

    def __str__(self):
        return self.question.title

    __sep__ = "||"

    @property
    def answer_only(self):
        if self.question.type in ['select', 'radio', 'checkbox']:
            return self.original_answer.replace(self.__sep__, ", ")
        return self.original_answer

    @property
    def answer_as_html(self):
        if self.question.type == "rating_radio":
            check_one = "checked" if self.original_answer in ["Awesome", "Good", "Average", "Bad", "4", "3", "2", "1" ] else ""
            check_two = "checked" if self.original_answer in ["Awesome", "Good", "Average", "4", "3", "2"] else ""
            check_three = "checked" if self.original_answer in ["Awesome", "Good", "4", "3"] else ""
            check_four = "checked" if self.original_answer in ["Awesome", "4" ] else ""
            return mark_safe(f"""
                <span class="fa fa-star {check_one}"></span>
                <span class="fa fa-star {check_two}"></span>
                <span class="fa fa-star {check_three}"></span>
                <span class="fa fa-star {check_four}"></span>
                """)

        if self.question.type == "checkbox":
            if "yes" in self.original_answer.lower():
                return mark_safe(f"""<span><i class="fa fa-check" aria-hidden="true"></i></span>""")
            elif "no" in self.original_answer.lower():
                return mark_safe(f"""<span><i class="fas fa-cross" aria-hidden="true"></i></span>""")
        return self.answer_only


@receiver(post_save, sender=Survey)
def create_sheet_signal(sender, created, instance, **kwargs):
    if created and instance.branch:
        from .google_sheets.main import create_sheet
        survey = f"{instance.branch}-{instance.name}"
        email = None
        sheet_id = create_sheet(survey)
        instance.spreadsheet_id = sheet_id
        instance.save()


# @receiver(post_save, sender=Answer)
# def write_sheet_signal(sender, created, instance, **kwargs):
#     if created:
#         from .google_sheets.main import write_sheet
#         key = instance.question.survey.spreadsheet_id
#         values = [instance.question.title, instance.original_answer, instance.question.type]
#         write_sheet(key, values)

@receiver(post_save, sender=Answer)
def write_sheet_signal(sender, created, instance, **kwargs):
    if (
            created
            and instance.question
            and instance.question.survey
            and instance.question.survey.concerned_person_contact
            and instance.question.send_sms_if_rating_in_marked
    ):
        _msg_to_send = False
        lines = instance.question.send_sms_if_rating_in_marked.split('\n')
        # import pdb;pdb.set_trace()
        for line in lines:
            line = line.split('\r')[0]  # new line has raw string representation \r
            if line in instance.original_answer.split(Answer.__sep__):
                _msg_to_send = line
                break
        if _msg_to_send and instance.group_id:
            url = reverse("redirect_to_admin", kwargs={"pk": instance.group_id})
            try:
                send_p_sms(instance.question.survey.concerned_person_contact,
                           message=f"Someone have reviewed '{_msg_to_send}' in your review system. "
                                   f"Refer Group: {instance.group.reference} https://feedback.demo.fegno.com{url}")
                instance.question.survey.url_opened_count += 1
                instance.question.survey.save()
            except Exception as e:
                pass



